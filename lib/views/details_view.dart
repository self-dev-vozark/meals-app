import 'package:flutter/material.dart';
import '../dummy/dummy_data.dart';

class DetailsView extends StatelessWidget {
  static const routeName = '/details';
  final Function(String) toggleFavorite;
  final Function(String) isFavorite;

  DetailsView(this.toggleFavorite, this.isFavorite);

  Widget buildTextSection(BuildContext ctx, String text) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        child: Text(
          text,
          style: Theme.of(ctx).textTheme.headline6,
        ));
  }

  Widget buildContainer(Widget child) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10)),
        height: 200,
        width: 300,
        child: child);
  }

  @override
  Widget build(BuildContext context) {
    final mealId = ModalRoute.of(context)?.settings.arguments as String;
    final meal = DUMMY_MEALS.firstWhere((element) => element.id == mealId);
    return Scaffold(
      appBar: AppBar(
        title: Text('${meal.name} Recipes'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(meal.imageUrl),
            ),
            buildTextSection(context, 'Ingredients'),
            buildContainer(ListView.builder(
              itemBuilder: (context, index) {
                return Card(
                    elevation: 2,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      child: Text(meal.ingredients[index]),
                    ));
              },
              itemCount: meal.ingredients.length,
            )),
            buildTextSection(context, 'Steps'),
            buildContainer(ListView.builder(
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    ListTile(
                      leading: CircleAvatar(
                        child: Text('#${index + 1}'),
                      ),
                      title: Text(meal.steps[index]),
                    ),
                    Divider()
                  ],
                );
              },
              itemCount: meal.steps.length,
            )),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(isFavorite(mealId) ? Icons.favorite : Icons.favorite_border),
        onPressed: () => toggleFavorite(mealId),
      ),
    );
  }
}
