import 'package:flutter/material.dart';
import 'package:meals/widgets/main_drawer.dart';

class FilterView extends StatefulWidget {
  static const routeName = '/filters';

  final Function(Map<String, bool>) setFilters;
  final Map<String, bool> currentFilters;

  FilterView(this.setFilters, this.currentFilters);

  @override
  _FilterViewState createState() => _FilterViewState();
}

class _FilterViewState extends State<FilterView> {
  bool _glutenFree = false;
  bool _lactoseFree = false;
  bool _vegan = false;
  bool _vegetarian = false;

  @override
  initState() {
    _glutenFree = widget.currentFilters['gluten']!;
    _lactoseFree = widget.currentFilters['lactose']!;
    _vegan = widget.currentFilters['vegan']!;
    _vegetarian = widget.currentFilters['vegetarian']!;
    super.initState();
  }

  Widget _buildSwitchList(String title, String subtitle, bool value, Function(bool) valueUpdate) {
    return SwitchListTile(
        title: Text(title), subtitle: Text(subtitle), value: value, onChanged: valueUpdate);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Filters'),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  widget.setFilters({
                    'gluten': _glutenFree,
                    'lactose': _lactoseFree,
                    'vegan': _vegan,
                    'vegetarian': _vegetarian
                  });
                },
                icon: Icon(Icons.save))
          ],
        ),
        drawer: MainDrawer(),
        body: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20),
              child: Text(
                'Adjust your meal selection',
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            Expanded(
                child: ListView(
              children: [
                _buildSwitchList('Gluten Free', 'Only include gluten-free meals', _glutenFree,
                    (value) {
                  setState(() {
                    _glutenFree = value;
                    print(_glutenFree);
                  });
                }),
                _buildSwitchList('Vegan', 'Only include vegan meals', _vegan, (value) {
                  setState(() {
                    _vegan = value;
                    print(_vegan);
                  });
                }),
                _buildSwitchList('Lactose Free', 'Only include lactose-free meals', _lactoseFree,
                    (value) {
                  setState(() {
                    _lactoseFree = value;
                  });
                }),
                _buildSwitchList('Vegetarian', 'Only include vegetarian meals', _vegetarian,
                    (value) {
                  setState(() {
                    _vegetarian = value;
                  });
                }),
              ],
            ))
          ],
        ));
  }
}
