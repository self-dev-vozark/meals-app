import 'package:flutter/material.dart';
import 'package:meals/models/meal.dart';
import 'package:meals/widgets/meal_item.dart';

class MealsView extends StatefulWidget {
  static const routeName = '/meals';
  final List<Meal> availableMeals;

  MealsView(this.availableMeals);

  @override
  _MealsViewState createState() => _MealsViewState();
}

class _MealsViewState extends State<MealsView> {
  String? categoryTitle;
  List<Meal>? meals;
  bool _loadedInitData = false;

  @override
  void didChangeDependencies() {
    if (_loadedInitData != true) {
      final routeArgs = ModalRoute.of(context)?.settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'] as String;
      meals = widget.availableMeals.where((meal) {
        return meal.categories.contains(routeArgs['id']);
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle!),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
              id: meals![index].id,
              name: meals![index].name,
              imageUrl: meals![index].imageUrl,
              duration: meals![index].duration,
              difficulty: meals![index].difficulty,
              price: meals![index].price);
        },
        itemCount: meals!.length,
      ),
    );
  }
}
