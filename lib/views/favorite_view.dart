import 'package:flutter/material.dart';
import 'package:meals/models/meal.dart';
import 'package:meals/widgets/meal_item.dart';

class FavoriteView extends StatelessWidget {
  final List<Meal> favoriteMeals;
  FavoriteView(this.favoriteMeals);

  @override
  Widget build(BuildContext context) {
    if (favoriteMeals.isEmpty) {
      return Center(child: Text('You have no favorite yet - try adding some!'));
    } else {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: favoriteMeals[index].id,
            name: favoriteMeals[index].name,
            imageUrl: favoriteMeals[index].imageUrl,
            duration: favoriteMeals[index].duration,
            difficulty: favoriteMeals[index].difficulty,
            price: favoriteMeals[index].price,
          );
        },
        itemCount: favoriteMeals.length,
      );
    }
  }
}
