import 'package:flutter/material.dart';
import 'package:meals/models/meal.dart';

class MealItem extends StatelessWidget {
  final String id;
  final String name;
  final String imageUrl;
  final int duration;
  final Difficulty difficulty;
  final Price price;

  MealItem(
      {required this.id,
      required this.name,
      required this.imageUrl,
      required this.duration,
      required this.difficulty,
      required this.price});

  String get difficultyText {
    switch (difficulty) {
      case Difficulty.Simple:
        return 'Simple';
      case Difficulty.Challenging:
        return 'Challenging';
      case Difficulty.Hard:
        return 'Hard';
      default:
        return 'Invalid';
    }
  }

  String get priceText {
    switch (price) {
      case Price.Affordable:
        return 'Affordable';
      case Price.Pricey:
        return 'Pricey';
      case Price.Luxurious:
        return 'Luxurious';
      default:
        return 'Invalid';
    }
  }

  void selectMeal(BuildContext ctx) {
    Navigator.of(ctx).pushNamed('/details', arguments: id);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () => selectMeal(context),
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          elevation: 5,
          margin: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                    child: Image.network(
                      imageUrl,
                      height: 250,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    bottom: 10,
                    right: 0,
                    child: Container(
                      color: Colors.black54,
                      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      width: 250,
                      child: Text(
                        name,
                        style: TextStyle(fontSize: 22, color: Colors.white),
                        softWrap: true,
                        overflow: TextOverflow.fade,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.schedule),
                        SizedBox(width: 2),
                        Text('$duration min')
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Icon(Icons.attach_money),
                        SizedBox(width: 2),
                        Text(priceText)
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Icon(Icons.work),
                        SizedBox(width: 2),
                        Text(difficultyText)
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
